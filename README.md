<p align="center">
  <a href="https://flutter.io/">
    <img src="https://storage.googleapis.com/cms-storage-bucket/6a07d8a62f4308d2b854.svg" alt="Logo" width=72 height=72>
  </a>

  <h3 align="center">Allapakuq App :seedling:</h3>

  <p align="center">
    MarketPlace for Primary Resource Providers and Consumers.
    <br>
    Base project made with much  :heart: . Contains CRUD, patterns, and much more!
    <br>
    <br>
    <a href="https://jhon-coronel-bautista.atlassian.net/jira/software/projects/AQ/boards/1">Jira board</a>
    ·
    <a href="https://bitbucket.org/allapakuq/app.mobile.allapakuq/jira">Jira issues</a>
  </p>
</p>

## Table of contents

- [Getting Started](#getting-started)
- [Copyright and license](#copyright-and-license)

## Getting Started

The repository code is preloaded with some basic components like basic app architecture, app theme, constants and required dependencies to create a new project.

## How to Use 

**Step 1:**

Download or clone this repo by using the link below:

```
git clone https://Jhon-cb@bitbucket.org/allapakuq/app.mobile.allapakuq.git
```

**Step 2:**

Go to project root and execute the following command in console to get the required dependencies: 

```
flutter pub get 
```

## Copyright and License
 
The MIT License (MIT)

Copyright (c) 2015 allapakuq

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Enjoy :gem: